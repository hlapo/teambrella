document.addEventListener('DOMContentLoaded', function(event) {
  console.info('[DOM Loaded]');

  $(window).scroll(function(){
    if ( $(window).scrollTop() > $('.navbar').outerHeight() ) {
      $('.navbar').addClass('tb-navbar--bg');
    } else {
      $('.navbar').removeClass('tb-navbar--bg');
    }
  });

  new ModalVideo('.js-video-vimeo-btn', {channel: 'vimeo'});

  $('.js-sale-countdown').countdown('2018/10/13', function(event) {
    $(this).find('.js-sale-days').text(event.strftime('%D'));
    $(this).find('.js-sale-hours').text(event.strftime('%H'));
    $(this).find('.js-sale-minutes').text(event.strftime('%M'));
    $(this).find('.js-sale-seconds').text(event.strftime('%S'));
  });

  var updateCurrentProgress = function() {
    var progress = 0;
    var items = $('.roadmap-timeline__cell.is-active');
    var isReady = false;
    var toNextLength = 25;
    $.each(items, function(index, item) {
      // console.log('currentIndex', index, 'of', items.length);
      if (index != items.length - 1) {
        progress += $(item).outerHeight(true);
      } else {
        // last iteration
        isReady = true;
        progress += toNextLength;
      }
      if (isReady) {
        // console.log('isReady', `${progress}px`);
        $('.js-timeline-animation').attr('style', `height: ${progress}px`);
        return true;
      }
    });
  }

  setTimeout(function() {
    updateCurrentProgress();
  }, 1000);


  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  $('.navbar').on('show.bs.collapse', function() {
    $(this).addClass('nav-collapse-bg');
  }).on('hide.bs.collapse', function() {
    $(this).removeClass('nav-collapse-bg');
  });

  console.info('[Helpers Loaded]');
});

