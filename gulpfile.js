const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const critical = require('critical').stream;
const gutil = require('gulp-util');
const fs = require('fs');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

gulp.task('styles', () => {
  return gulp
    .src('app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe(
      $.sass
        .sync({
          outputStyle: 'expanded',
          precision: 10,
          includePaths: ['.']
        })
        .on('error', $.sass.logError)
    )
    .pipe($.autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'] }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({ stream: true }));
});

gulp.task('scripts', () => {
  return gulp
    .src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({ stream: true }));
});

function lint(files, options) {
  return gulp
    .src(files)
    .pipe(reload({ stream: true, once: true }))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js', {
    fix: true
  }).pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js', {
    fix: true,
    env: {
      mocha: true
    }
  }).pipe(gulp.dest('test/spec/**/*.js'));
});

gulp.task('html', ['views', 'styles', 'scripts'], () => {
  return (
    gulp
      .src(['app/*.html', '.tmp/*.html'])
      .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
      .pipe($.if('*.js', $.uglify()))
      .pipe($.if('*.css', $.cssnano({ safe: true, autoprefixer: false })))
      .pipe($.if('*.html', $.htmlmin({ collapseWhitespace: true })))
      .pipe(gulp.dest('dist'))
  );
});

gulp.task('images', () => {
  return gulp
    .src('app/images/**/*')
    // .pipe(
    //   $.cache(
    //     $.imagemin({
    //       progressive: true,
    //       interlaced: true,
    //       // don't remove IDs from SVGs, they are often used
    //       // as hooks for embedding and styling
    //       svgoPlugins: [{ cleanupIDs: false }]
    //     })
    //   )
    // )
    .pipe(gulp.dest('dist/images'));
});

gulp.task('extras', () => {
  return gulp
    .src(['app/*.*', '!app/*.html', '!app/*.pug'], {
      dot: true
    })
    .pipe(gulp.dest('dist'));
});

gulp.task('fonts', () => {
  return gulp.src(['app/fonts/**']).pipe(gulp.dest('dist/fonts'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['views', 'styles', 'scripts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch(['app/*.html', 'app/images/**/*']).on('change', reload);

  gulp.watch('app/**/*.pug', ['views']);
  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.js', ['scripts']);
});

gulp.task('serve:dist', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

gulp.task('build', ['lint', 'html', 'images', 'extras', 'fonts'], () => {
  return gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true }));
});

gulp.task('default', ['clean'], () => {
  gulp.start('compress');
});

gulp.task('views', () => {
  return gulp
    .src('app/*.pug')
    .pipe($.plumber())
    .pipe($.data(function() {
      return JSON.parse(fs.readFileSync('app/timeline.json'));
    }))
    .pipe(
      $.pug({
        pretty: true,
        locals: {
          icon: name => {
            return fs.readFileSync(`app/icons/${name}.svg`);
          }
        }
      })
    )
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({ stream: true }));
});

/*gulp.task('critical', ['html'], () => {
  return gulp
    .src('dist/*.html')
    .pipe(
      critical({
        base: 'dist/',
        inline: true,
        css: ['dist/styles/app.css']
      })
    )
    .on('error', function(err) {
      gutil.log(gutil.colors.red(err.message));
    })
    .pipe(gulp.dest('dist'));
});*/

gulp.task('compress', ['build'], () => {
  let zipPackageName = require('path').basename(__dirname);
  let timeStamp = new Date().getTime();
  return gulp
    .src('dist', { base: '.' })
    .pipe($.zip(zipPackageName + '_' + timeStamp + '.zip'))
    .pipe(gulp.dest('release'));
});
