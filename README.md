# Starter Kit Template
To build project run `gulp` and check `release/`

## Processing

Pug -> HTML

Scss -> Autoprefixer -> CriticalCSS -> CSS

## Development mode
`gulp serve`

## Build
`gulp` will produce `release/project_name.zip`

**Note: to get better performance un-comment minifiers in `gulpfile.js` in `html` task.**
```
// .pipe($.if("*.js", $.uglify()))
// .pipe($.if("*.css", $.cssnano({ safe: true, autoprefixer: false })))
// .pipe($.if("*.html", $.htmlmin({ collapseWhitespace: true }));
```

## SVG
Add `*.svg` icon to `app/icons` and use `+icon('filename')` in `*.pug` template.

## To encode video
If you need to reencode the video try
`brew reinstall ffmpeg --with-libvpx`
`ffmpeg -i input.mp4 -c copy -an -c:v libx264 -b:v 2000000 output.mp4`
`ffmpeg -i input.mp4 -c copy -an -c:v libvpx -b:v 2000000 output.webm`

## Get first frame of video for poster image
`ffmpeg -ss 0 -i cloud.mp4 -frames:v 1 out.jpg`

## Convert images for the frontpage
convert app/images/jumbotron.jpg -resize '1440x900^' +repage -gravity center -extent 1440x900 -strip -density 72 app/images/jumbotron_new.jpg


## Authors

@denysbutenko

@hlaposhka
